#!/bin/bash

docker run -d --restart unless-stopped --name dnsmasq --cap-add=NET_ADMIN --net=host \
	-v "$PWD/config/dnsmasq.d":/etc/dnsmasq.d \
	-v "$PWD/config/dnsmasq.conf":/etc/dnsmasq.conf \
	-v "$PWD/config/hosts":/etc/hosts \
	dnsmasq
