FROM alpine:3.5

RUN apk --no-cache add dnsmasq

EXPOSE 53 53/udp

CMD ["dnsmasq", "-k", "--conf-file=/etc/dnsmasq.conf", "--log-queries", "--log-facility=/var/log/dnsmasq.log"]
