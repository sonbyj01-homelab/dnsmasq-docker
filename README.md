# dnsmasq docker
This is the docker container I host for my homelab set up.

Learn more about [dnsmasq] and the different configurations.

## Config Files
*hosts* - assign hostnames to ip addresses (i.e. "192.168.1.1 router router.example.com") 

*dnsmasq.conf* - dnsmasq configurations, refer to website for changes

*dnsmasq.d/dhcp.hosts* - allows you to manually assign ip addresses and hostnames to machines according to MAC addresses

*dnsmasq.d/dhcp.conf* - dnsmasq dhcp configurations, refer to website for changes

[dnsmasq]:http://www.thekelleys.org.uk/dnsmasq/doc.html
